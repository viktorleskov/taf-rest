package ua.com.epam.testobject.bo;

import org.testng.Assert;
import ua.com.epam.entity.author.Author;
import ua.com.epam.testobject.BaseTest;

import java.util.List;

import static ua.com.epam.config.URI.*;
import static ua.com.epam.config.URI.PUT_AUTHOR_SINGLE_OBJ;

public class AuthorCRUDBO extends BaseTest {
    public Author postRandomAuthor(){
        return postAuthor(testData.authors().getRandomOne());
    }

    public List<Author> postRandomAuthors10(){
        List<Author> authors = testData.authors().getDefaultAuthors();
        for(int k=0; k < authors.size(); k++){
            postAuthor(authors.get(k));
        }
        return authors;
    }

    public Author postAuthor(Author expA) {
        client.post(POST_AUTHOR_SINGLE_OBJ, expA); // post author
        String body = client.getResponse().getBody();          // get body from response as String

        int statusCode = client.getResponse().getStatusCode(); // get response status code
        Author actA = g.fromJson(body, Author.class);          // map response String to Author obj

        Assert.assertEquals(statusCode, 201);           // verify that statusCode is 201
        Assert.assertEquals(actA, expA);                       // verify that Author object is equal to expected
        return actA;
    }

    public Author getAuthorById(Long id) {
        client.get(String.format(GET_AUTHOR_SINGLE_OBJ, id)); //get author
        String body = client.getResponse().getBody();         //get body from response

        int statusCode = client.getResponse().getStatusCode();//get status code
        Author actA = g.fromJson(body, Author.class);         //map response to Author obj

        Assert.assertEquals(statusCode, 200);        //verify status code
        Assert.assertEquals(actA.getAuthorId().toString(), id.toString());     // verify that Author object is equal to expected
        return actA;
    }

    public AuthorCRUDBO deleteAuthorById(Long id){
        client.delete(String.format(DELETE_AUTHOR_SINGLE_OBJ,id.toString())); //delete author
        int statusCode = client.getResponse().getStatusCode();     //get status code

        Assert.assertEquals(statusCode, 204);        //verify status code
        return this;
    }

    public AuthorCRUDBO putAuthorById(Long id, Author body){
        Author buffer = new Author(id, body);
        client.put(String.format(PUT_AUTHOR_SINGLE_OBJ, id), buffer); //put author
        String responseBody = client.getResponse().getBody();         //get body from response

        int statusCode = client.getResponse().getStatusCode();  //get status code

        Assert.assertEquals(statusCode, 200);
        Assert.assertEquals(g.fromJson(responseBody,Author.class), buffer);
        return this;
    }



}
