package ua.com.epam.testobject.bo;

import org.testng.Assert;
import ua.com.epam.entity.genre.Genre;
import ua.com.epam.testobject.BaseTest;

import java.util.List;

import static ua.com.epam.config.URI.*;

public class GenreCRUDBO extends BaseTest {
    public Genre postRandomGenre(){
        return postGenre(testData.genres().getRandomOne());
    }

    public List<Genre> postRandomGenres10(){
        List<Genre> genres = testData.genres().getDefaultGenre();
        for(int k=0; k < genres.size(); k++){
            postGenre(genres.get(k));
        }
        return genres;
    }

    public Genre postGenre(Genre expA) {
        client.post(POST_GENRE_SINGLE_OBJ, expA); // post genre
        String body = client.getResponse().getBody();          // get body from response as String

        int statusCode = client.getResponse().getStatusCode(); // get response status code
        Genre actA = g.fromJson(body, Genre.class);          // map response String to Genre obj

        Assert.assertEquals(statusCode, 201);           // verify that statusCode is 201
        Assert.assertEquals(actA, expA);                       // verify that Genre object is equal to expected
        return actA;
    }

    public Genre postGenreToAuthor(Genre expA, Long authorId) {
        client.post(String.format(POST_GENRE_TO_AUTHOR,authorId.toString()), expA); // post genre
        String body = client.getResponse().getBody();          // get body from response as String

        int statusCode = client.getResponse().getStatusCode(); // get response status code
        Genre actA = g.fromJson(body, Genre.class);          // map response String to Genre obj

        Assert.assertEquals(statusCode, 201);           // verify that statusCode is 201
        Assert.assertEquals(actA, expA);                       // verify that Genre object is equal to expected
        return actA;
    }

    public Genre getGenreById(Long id) {
        client.get(String.format(GET_GENRE_SINGLE_OBJ, id)); //get genre
        String body = client.getResponse().getBody();         //get body from response

        int statusCode = client.getResponse().getStatusCode();//get status code
        Genre actA = g.fromJson(body, Genre.class);         //map response to Author obj

        Assert.assertEquals(statusCode, 200);        //verify status code
        Assert.assertEquals(actA.getGenreId().toString(), id.toString());        // verify that Genre object is equal to expected
        return actA;
    }

    public GenreCRUDBO deleteGenreById(Long id){
        client.delete(String.format(DELETE_GENRE_SINGLE_OBJ,id.toString())); //delete genre
        int statusCode = client.getResponse().getStatusCode();     //get status code

        Assert.assertEquals(statusCode, 204);        //verify status code
        return this;
    }

    public GenreCRUDBO putGenreById(Long id, Genre body){
        Genre buffer= new Genre(id, body);
        client.put(String.format(PUT_AUTHOR_SINGLE_OBJ, id), buffer); //put genre
        String responseBody = client.getResponse().getBody();         //get body from response

        int statusCode = client.getResponse().getStatusCode();  //get status code

        Assert.assertEquals(statusCode, 200);
        Assert.assertEquals(g.fromJson(responseBody,Genre.class), buffer);
        return this;
    }

}
