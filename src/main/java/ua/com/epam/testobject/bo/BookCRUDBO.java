package ua.com.epam.testobject.bo;

import org.testng.Assert;
import ua.com.epam.entity.author.Author;
import ua.com.epam.entity.book.Book;
import ua.com.epam.entity.genre.Genre;
import ua.com.epam.testobject.BaseTest;

import java.util.List;

import static ua.com.epam.config.URI.*;

public class BookCRUDBO extends BaseTest {
    AuthorCRUDBO wannaTestAuthor = new AuthorCRUDBO();
    GenreCRUDBO wannaTestGenre = new GenreCRUDBO();

    public Book postRandomBook(Author author,Genre genre){
        return postBook(testData.books().getRandomOne(),author,genre);
    }

    public List<Book> postRandomBooks10(List<Author>autors, List<Genre>genres){
        List<Book> books = testData.books().getDefaultBook();
        for(int k=0; k < books.size(); k++){
            postBook(books.get(k),
                    autors.get((int)Math.random()+autors.size()-1),
                    genres.get((int)Math.random()+genres.size()-1));
        }
        return books;
    }

    public Book postBook(Book expA, Author author, Genre genre) {
        wannaTestGenre.postGenreToAuthor(genre, author.getAuthorId());

        client.post(String.format(POST_BOOK_SINGLE_OBJ, author.getAuthorId(), genre.getGenreId()), expA); // post book
        String body = client.getResponse().getBody();          // get body from response as String

        int statusCode = client.getResponse().getStatusCode(); // get response status code
        Book actA = g.fromJson(body, Book.class);          // map response String to Book obj

        Assert.assertEquals(statusCode, 201);           // verify that statusCode is 201
        Assert.assertEquals(actA, expA);                       // verify that Genre object is equal to expected
        return actA;
    }

    public Book getBookById(Long id) {
        client.get(String.format(GET_BOOK_SINGLE_OBJ, id)); //get book
        String body = client.getResponse().getBody();         //get body from response

        int statusCode = client.getResponse().getStatusCode();//get status code
        Book actA = g.fromJson(body, Book.class);         //map response to Book obj

        Assert.assertEquals(statusCode, 200);        //verify status code
        Assert.assertEquals(actA.getBookId().toString(), id.toString());        // verify that Book object is equal to expected
        return actA;
    }

    public BookCRUDBO deleteBookById(Long id){
        client.delete(String.format(DELETE_BOOK_SINGLE_OBJ,id.toString())); //delete book
        int statusCode = client.getResponse().getStatusCode();     //get status code

        Assert.assertEquals(statusCode, 204);        //verify status code
        return this;
    }

    public BookCRUDBO putBookById(Long id, Book body){
        Book buffer= new Book(id, body);
        client.put(String.format(PUT_BOOK_SINGLE_OBJ, id), buffer); //put book
        String responseBody = client.getResponse().getBody();         //get body from response

        int statusCode = client.getResponse().getStatusCode();  //get status code

        Assert.assertEquals(statusCode, 200);
        Assert.assertEquals(g.fromJson(responseBody,Book.class), buffer);
        return this;
    }

}
