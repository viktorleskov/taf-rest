package ua.com.epam.config;

import ua.com.epam.utils.data.BaseData;

public interface URI {
    DataProp dp = new DataProp();
    String BASE_URI = dp.apiProtocol() + "://" + dp.apiHost() + ":" + dp.apiPort();

    //Author
    String GET_AUTHOR_SINGLE_OBJ = BASE_URI + "/api/library/author/%s";
    String GET_AUTHOR_OF_BOOK_OBJ = BASE_URI + "/api/library/book/%s/author";
    String GET_ALL_AUTHORS_ARR = BASE_URI + "/api/library/authors";
    String POST_AUTHOR_SINGLE_OBJ = BASE_URI + "/api/library/author/new";
    String POST_GENRE_TO_AUTHOR = BASE_URI + "/api/library/author/%s/new";
    String PUT_AUTHOR_SINGLE_OBJ = BASE_URI + "/api/library/author/%s/update";
    String DELETE_AUTHOR_SINGLE_OBJ = BASE_URI + "/api/library/author/%s/delete";

    //Book
    String GET_BOOK_SINGLE_OBJ = BASE_URI + "/api/library/book/%s";  //by bookId
    String GET_ALL_BOOKS_ARR = BASE_URI + "/api/library/books";   //all books
    String GET_ALL_BOOKS_BY_AUTHOR = BASE_URI + "/api/library/author/%s/books"; //all books by authorId
    String GET_ALL_BOOKS_BY_AUTHOR_AND_GENRE = BASE_URI + "/api/library/author/%s/genre/%s/books"; //Author Id/Genre Id
    String GET_ALL_BOOKS_BY_GENRE = BASE_URI + "/api/library/genre/%s/books"; //genreId
    String POST_BOOK_SINGLE_OBJ = BASE_URI + "/api/library/book/%s/%s/new"; //authorID/genreId
    String PUT_BOOK_SINGLE_OBJ = BASE_URI + "/api/library/book/%s/update"; //bookId
    String DELETE_BOOK_SINGLE_OBJ = BASE_URI + "/api/library/book/%s/delete"; //bookId

    //Genre
    String GET_GENRE_SINGLE_OBJ = BASE_URI + "/api/library/genre/%s"; //genreId
    String GET_ALL_GENRES_ARR = BASE_URI + "/api/library/genres"; //all genres arraylist
    String GET_GENRE_OF_BOOK = BASE_URI + "/api/library/book/%s/genre"; //bookId
    String POST_GENRE_SINGLE_OBJ = BASE_URI + "/api/library/genre/new"; //post genre
    String PUT_GENRE_SINGLE_OBJ = BASE_URI + "/api/library/genre/%s/update"; //genreId
    String DELETE_GENRE_SINGLE_OBJ = BASE_URI + "/api/library/genre/%s/delete"; //genreId
}
