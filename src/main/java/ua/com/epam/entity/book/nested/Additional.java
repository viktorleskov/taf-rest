package ua.com.epam.entity.book.nested;

import java.util.Objects;

public class Additional {
    private Long pageCount;
    private Size size;

    public Additional(){
    }
    public Additional(Long pagecount){
        this.pageCount = pagecount;
    }
    public Additional(long hei, long len, long width, long pc){
        this.pageCount=pc;
        this.size = new Size(hei,len,width);
    }

    public Long getPageCount(){
        return pageCount;
    }
    public void setPageCount(Long pagecount){
        this.pageCount=pagecount;
    }
    public Size getSize(){return size;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Additional additional = (Additional) o;
        return Objects.equals(pageCount, additional.pageCount)&&
                Objects.equals(size,additional.size);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pageCount,size);
    }

    public class Size{
        private Long height;
        private Long length;
        private Long width;

        public Size(){
        }
        public Size(Long h,Long l, Long w){
            this.height =h;
            this.length =l;
            this.width =w;
        }
        public Long getHeight(){
            return height;
        }
        public Size setHeight(Long h){
            this.height=h;
            return this;
        }
        public Long getLength(){
            return length;
        }
        public Size setLength(Long l){
            this.length =l;
            return this;
        }
        public Long getWidth(){
            return width;
        }
        public Additional setWidth(Long w){
            this.width =w;
            return Additional.this;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Size size = (Size) o;
            return Objects.equals(height, size.height)&&
                    Objects.equals(length,size.length)&&
                    Objects.equals(width,size.width);
        }

        @Override
        public int hashCode() {
            return Objects.hash(height,length,width);
        }
    }


}
