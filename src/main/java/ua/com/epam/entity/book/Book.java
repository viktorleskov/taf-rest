package ua.com.epam.entity.book;

import ua.com.epam.entity.book.nested.Additional;

import java.util.Objects;

public class Book {
    private Long bookId;
    private String bookName;
    private String bookLanguage;
    private String bookDescription;
    private Long publicationYear;
    private Additional information;

    public Book() {
        information = new Additional();
    }

    public Book(Long bookId, String bookName, String bookLanguage, String bookDescription, Additional info, Long publicationYear) {
        this.bookId = bookId;
        this.bookName = bookName;
        this.bookLanguage = bookLanguage;
        this.bookDescription = bookDescription;
        this.information = info;
        this.publicationYear = publicationYear;
    }
    public Book(Long bookId, Book copy){
        this.bookId = bookId;
        this.bookName = copy.getBookName();
        this.bookLanguage = copy.getLanguage();
        this.bookDescription = copy.getBookDescription();
        this.information = copy.getInformation();
        this.publicationYear = copy.getPublicationYear();
    }

    public Long getBookId() {
        return bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public Book setBookName(String bookName) {
        this.bookName = bookName;
        return this;
    }

    public String getLanguage() {
        return bookLanguage;
    }

    public Book setLanguage(String language) {
        this.bookLanguage = language;
        return this;
    }

    public String getBookDescription() {
        return bookDescription;
    }

    public void setBookDescription(String description) {
        this.bookDescription = description;
    }

    public Long getPublicationYear() {
        return publicationYear;
    }

    public Book setPublicationYear(Long year) {
        this.publicationYear = year;
        return this;
    }

    public Additional getInformation(){
        return this.information;
    }

    public Book setInformation(Additional info){
        this.information=info;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(bookId, book.bookId) &&
                Objects.equals(bookName, book.bookName) &&
                Objects.equals(bookLanguage, book.bookLanguage) &&
                Objects.equals(bookDescription, book.bookDescription) &&
                Objects.equals(publicationYear, book.publicationYear);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookId, bookName, bookLanguage, bookDescription, publicationYear);
    }
}
