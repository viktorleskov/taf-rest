package ua.com.epam.utils.data.service;

import org.apache.log4j.Logger;
import ua.com.epam.entity.book.Book;
import ua.com.epam.entity.book.nested.Additional;
import ua.com.epam.utils.data.BaseData;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static ua.com.epam.utils.JsonKeys.*;
import static ua.com.epam.utils.helpers.SqlQuery.SELECT_DEFAULTS;
import static ua.com.epam.utils.helpers.SqlQuery.*;

public class BookData extends BaseData {
    private static Logger log = Logger.getLogger(BookData.class);

    //will get one random author from our data base with test data
    public Book getRandomOne() {
        log.info("Try to find one random book...");
        execute(String.format(SELECT_RANDOM_ONE, dp.dbName(), BOOK));
        Book b = new Book();
        try {
            if (!resultSet.next()) {
                log.error("No one book was found! Book table is empty!");
            } else {
                b = mapResultSetObjToBook(resultSet);
                log.info("Book with bookId = " + b.getBookId() + " was found!\n");
            }
        } catch (SQLException e) {
            log.error("DB access error occurs or method is called on a closed ResultSet!!!");
            e.printStackTrace();
        }

        close();
        return b;
    }

    //return 10 authors sortedBy authorId in ascending order
    public List<Book> getDefaultBook() {
        log.info("Try to find first 10 boooks...");
        execute(String.format(SELECT_DEFAULTS, dp.dbName(), BOOK, BOOK_ID));
        List<Book> books = new ArrayList<>();
        try {
            int i = 0;
            if (!resultSet.next()) {
                log.error("No one book was found! Book table is empty!");
            } else {
                do {
                    books.add(mapResultSetObjToBook(resultSet));
                    i++;
                } while (resultSet.next());
                log.info("Found " + i + " book successfully!");
            }
        } catch (SQLException e) {
            log.error("DB access error occurs or method is called on a closed ResultSet!!!");
            e.printStackTrace();
        }

        close();
        return books;
    }

    //and here you can specify by what parameter our authors will be sorted by
    //also you can specify sorting order
    //and optional parameter - count; If you miss this will set as 10
    public List<Book> getSorted(String sortBy, String order, int... count) {
        int limit = count.length == 0 ? 10 : count[0];

        log.info("Try to find first " + limit + " books sorted by " + sortBy + " in " + order + " order...");
        execute(String.format(SELECT_CUSTOMS, dp.dbName(), BOOK, sortBy, order, limit));
        List<Book> books = new ArrayList<>();

        try {
            int i = 0;
            if (!resultSet.next()) {
                log.error("No one book was found! Book table is empty!");
            } else {
                do {
                    books.add(mapResultSetObjToBook(resultSet));
                    i++;
                } while (resultSet.next());
                log.info("Found " + i + " books successfully!\n");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        close();
        return books;
    }

    private Book mapResultSetObjToBook(ResultSet rs) throws SQLException {
        return new Book(
                rs.getLong(BOOK_ID),
                rs.getString(BOOK_NAME),
                rs.getString(BOOK_LANGUAGE),
                rs.getString(BOOK_DESCRIPTION),
                new Additional(
                        rs.getLong(BOOK_SIZE_HEIGHT)
                        ,rs.getLong(BOOK_SIZE_LENGTH)
                        ,rs.getLong(BOOK_SIZE_WIDTH)
                        ,rs.getLong(BOOK_PAGECOUNT)),
                rs.getLong(BOOK_PUBLICATION_YEAR));
    }
}
