package ua.com.epam.crud;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import ua.com.epam.entity.genre.Genre;
import ua.com.epam.testobject.BaseTest;
import ua.com.epam.testobject.bo.GenreCRUDBO;

import java.util.List;
import java.util.ListIterator;

@Test(description = "CRUD operations for genre table")
public class CRUDGenreTest extends BaseTest{
        private GenreCRUDBO wannaTest = new GenreCRUDBO();

        @Test(description = "Post Genre test")
        public void postGenreTest() {
            wannaTest
                    .postRandomGenre();
            wannaTest
                    .postRandomGenres10();
        }

        @Test(description = "Get genre test")
        public void getGenreTest() {
            List<Genre> genres = wannaTest.postRandomGenres10();
            ListIterator<Genre>
                    iterator = genres.listIterator();

            while(iterator.hasNext()){
                wannaTest
                        .getGenreById(iterator.next().getGenreId());
            }
        }

        @Test(description = "Delete genre test")
        public void deleteGenreTest(){
            List<Genre> genres = wannaTest.postRandomGenres10();
            ListIterator<Genre>
                    iterator = genres.listIterator();

            while(iterator.hasNext()){
                wannaTest
                        .deleteGenreById(iterator.next().getGenreId());
            }
        }

        @Test(description = "Put genre test")
        public void putAuthorTest(){
            Genre general = wannaTest.postRandomGenre();
            List<Genre>genres = wannaTest.postRandomGenres10();
            ListIterator<Genre>
                    iterator = genres.listIterator();

            while(iterator.hasNext()){
                wannaTest
                        .putGenreById(iterator.next().getGenreId(),general);
            }
        }

        @AfterMethod(alwaysRun = true)
        public void cleanUp() {
            clean.genres();
        }

    }

