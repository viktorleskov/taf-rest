package ua.com.epam.crud;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import ua.com.epam.entity.author.Author;
import ua.com.epam.testobject.BaseTest;
import ua.com.epam.testobject.bo.AuthorCRUDBO;

import java.util.List;
import java.util.ListIterator;


@Test(description = "CRUD operations for author table")
public class CRUDAuthorTest extends BaseTest{
    private AuthorCRUDBO authorCRUDBO = new AuthorCRUDBO();

    @Test(description = "Post author test")
    public void postAuthorTest() {
        authorCRUDBO.postRandomAuthor();
    }

    @Test(description = "Get author test")
    public void getAuthorTest() {
        List<Author> authors = authorCRUDBO.postRandomAuthors10();
        ListIterator<Author>
                iterator = authors.listIterator();

       while(iterator.hasNext()){
           authorCRUDBO
                   .getAuthorById(iterator.next().getAuthorId());
       }

    }

    @Test(description = "Delete author test")
    public void deleteAuthorTest(){
        List<Author> authors = authorCRUDBO.postRandomAuthors10();
        ListIterator<Author>
                iterator = authors.listIterator();

        while(iterator.hasNext()){
            authorCRUDBO
                    .deleteAuthorById(iterator.next().getAuthorId());
        }
    }

    @Test(description = "Put author test")
    public void putAuthorTest(){
        Author general = authorCRUDBO.postRandomAuthor();
        List<Author> authors = authorCRUDBO.postRandomAuthors10();
        ListIterator<Author>
                iterator = authors.listIterator();

        while(iterator.hasNext()){
            authorCRUDBO
                    .putAuthorById(iterator.next().getAuthorId(),general);
        }
    }

    @AfterMethod(alwaysRun = true)
    public void cleanUp() {
        clean.authors();
    }

}
