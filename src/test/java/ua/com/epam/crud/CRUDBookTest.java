package ua.com.epam.crud;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import ua.com.epam.entity.author.Author;
import ua.com.epam.entity.genre.Genre;
import ua.com.epam.testobject.BaseTest;
import ua.com.epam.testobject.bo.AuthorCRUDBO;
import ua.com.epam.testobject.bo.BookCRUDBO;
import ua.com.epam.testobject.bo.GenreCRUDBO;

import java.util.List;

@Test(description = "CRUD operations for book table")
public class CRUDBookTest extends BaseTest {
    private BookCRUDBO bookCRUDBO = new BookCRUDBO();
    private AuthorCRUDBO authorCRUDBO = new AuthorCRUDBO();
    private GenreCRUDBO genreCRUDBO = new GenreCRUDBO();

    @Test(description = "Post Book by genre and author test")
    public void postBookTest() {
        List<Author> authors = authorCRUDBO.postRandomAuthors10();
        List<Genre> genres = genreCRUDBO.postRandomGenres10();
        bookCRUDBO.postRandomBooks10(authors,genres);
    }

    @Test(description = "Get Book test")
    public void getBookTest() {
//       /* List<Book> books = wannaTest.postRandomBooks10();
//        ListIterator<Book>
//                iterator = books.listIterator();
//
//        while(iterator.hasNext()){
//            wannaTest
//                    .getBookById(iterator.next().getBookId());
//        }*/

    }

    @Test(description = "Delete Book test")
    public void deleteBookTest(){
       /* List<Book> books = wannaTest.postRandomBooks10();
        ListIterator<Book>
                iterator = books.listIterator();

        while(iterator.hasNext()){
            wannaTest
                    .deleteBookById(iterator.next().getBookId());
        }*/
    }

    @Test(description = "Put book test")
    public void putBookTest(){
       /* Book general = wannaTest.postRandomBook();
        List<Book> books = wannaTest.postRandomBooks10();
        ListIterator<Book>
                iterator = books.listIterator();

        while(iterator.hasNext()){
            wannaTest
                    .putBookById(iterator.next().getBookId(),general);
        }*/
    }

    @AfterMethod(alwaysRun = true)
    public void cleanUp() {
        clean.authors();
        clean.genres();
        clean.books();
    }

}
